import { composeWithDevTools } from "redux-devtools-extension";
import { createStore, combineReducers, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import * as ProjectReducer from "./redux/reducer/project"
import * as NeedReducer from "./redux/reducer/need"

const ConfigureStore = () => {

    const reducer = combineReducers({
      project: ProjectReducer.reducer,
      need: NeedReducer.reducer
    })

    return createStore(
      reducer,
      {
        project: ProjectReducer.initialState,
        need: NeedReducer.initialState
      },
      composeWithDevTools(applyMiddleware(thunk))
    );

}

export default ConfigureStore;
