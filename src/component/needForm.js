import React, { useState } from "react";
import { Grid, Paper, TextField, Button } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import { addNeed } from "./../redux/action/need";
import { connect } from "react-redux";
import { useForm } from "react-hook-form";

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        color: theme.palette.text.secondary,
        marginBottom: theme.spacing(2),
    },
    button: {
        marginTop: theme.spacing(1),
        marginLeft: theme.spacing(5),
    }
}));

const Need = props => {

    const classes = useStyles();
    const [colapseDescription, setColapseDescription] = useState(false);
    const { register, handleSubmit, errors } = useForm();

    const onSubmit = (data, e) => {
      console.log(data)
      props.addNeed(data);
      e.target.reset();
    }

    return (
      <form onSubmit={handleSubmit(onSubmit)}>
        <Paper className={classes.paper}>
          <Grid container>
            <Grid item xs={10} style={{ textAlign: "right" }}>
              <TextField 
                label="Besoin" 
                fullWidth 
                inputRef={register({required: true})} 
                error={errors.title && true}
                name='title'/>
              <Button
                size="small"
                startIcon={<ArrowDropDownIcon size="small" />}
                onClick={() => setColapseDescription(!colapseDescription)}
              >
                description
              </Button>
              <TextField
                name="description"
                style={{ display: colapseDescription ? "" : "none" }}
                label="Description"
                fullWidth
                inputRef={register}
              />
            </Grid>
            <Grid item xs={2}>
              <Button
                variant="outlined"
                color="primary"
                className={classes.button}
                type="submit"
              >
                Ajouter
              </Button>
            </Grid>
          </Grid>
        </Paper>
      </form>
    );
}

export default connect(null, { addNeed })(Need);
