import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Divider from "@material-ui/core/Divider";
import ExpansionPanelActions from "@material-ui/core/ExpansionPanelActions";
import Button from "@material-ui/core/Button";
import Chip from "@material-ui/core/Chip";
import Grid from '@material-ui/core/Grid';
import NeedTabs from "./needTabs";


const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        fontWeight: theme.typography.fontWeightRegular,
  },
  helper: {
    borderLeft: `2px solid ${theme.palette.divider}`,
    padding: theme.spacing(1, 2),
  },
}));

const SimpleExpansionPanel = props => {
    const classes = useStyles();

    return (
      <ExpansionPanel>
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <Typography className={classes.heading}>
            {props.need.title}
            <Chip
              style={{ marginLeft: "10px" }}
              label={props.need.status}
              variant="outlined"
              size="small"
            />
          </Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <Grid container>
            <Grid item xs={4}>
              <Typography>{props.need.description}</Typography>
            </Grid>
            <Grid item xs={8} className={classes.helper}>
              <NeedTabs/>
            </Grid>
          </Grid>
        </ExpansionPanelDetails>
        <Divider />
        <ExpansionPanelActions>
          {"draft" === props.need.status && (
            <>
              <Button size="small" color="secondary">
                Supprimer
              </Button>
              <Button size="small" color="primary">
                Soumettre
              </Button>
            </>
          )}
        </ExpansionPanelActions>
      </ExpansionPanel>
    );
}

export default SimpleExpansionPanel;
