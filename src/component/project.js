import React from "react";
import { Grid, Paper, TextField, Button } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import { updateName } from "./../redux/action/project";

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        color: theme.palette.text.secondary,
        marginBottom: theme.spacing(5),
        marginTop: theme.spacing(3),
    },
    button: {
        marginTop: theme.spacing(1),
        marginLeft: theme.spacing(5),
    }
}));

const Project = props => {

    const classes = useStyles();

    return (
      <form>
        <Paper className={classes.paper}>
          <Grid container>
            <Grid item xs={10} style={{ textAlign: "right" }}>
              <TextField label="Projet" fullWidth onChange={ e => props.updateName(e.target.value) } />
            </Grid>
            <Grid item xs={2}>
              <Button
                variant="outlined"
                color="primary"
                className={classes.button}
              >
                Enregistrer
              </Button>
            </Grid>
          </Grid>
        </Paper>
      </form>
    );
}

export default connect(null, { updateName })(Project);
