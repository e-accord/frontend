import React, { useState } from "react";
import AceEditor from "react-ace";
import Paper from "@material-ui/core/Paper";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";

import "ace-builds/src-noconflict/mode-javascript";
import "ace-builds/src-noconflict/theme-github";

function a11yProps(index) {
  return {
    id: `wrapped-tab-${index}`,
    "aria-controls": `wrapped-tabpanel-${index}`,
  };
}

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`wrapped-tabpanel-${index}`}
      aria-labelledby={`wrapped-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          {children}
        </Box>
      )}
    </div>
  );
}

const NeedTabs = () => {

    const [value, setValue] = useState("resource");
    const [jsonResource, setJsonResource] = useState("");

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    return (
      <Paper square>
        <Tabs
          indicatorColor="primary"
          textColor="primary"
          onChange={handleChange}
        >
          <Tab value="resource" label="Ressource" {...a11yProps("resource")} />
          <Tab
            value="comments"
            label="Commentaires"
            {...a11yProps("comments")}
          />
        </Tabs>
        <TabPanel value={value} index="resource">
          <AceEditor
            value={jsonResource}
            onChange={value => setJsonResource(value)}
            mode="javascript"
            theme="github"
            name="resource-template"
            height="200px"
            style={{ marginBottom: "15px" }}
          />
          <Button variant="outlined" color="success">
            Enregistrer
          </Button>
        </TabPanel>
        <TabPanel value={value} index="comments">
          Comments
        </TabPanel>
      </Paper>
    );
}

export default NeedTabs;