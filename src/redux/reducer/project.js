import * as type from "../action/projectType";

const initialState = {
    name: null
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case type.PROJECT_SAVE:
            return {...action.payload}
        
        case type.PROJECT_UPDATE_NAME:
            return { ...state, name: action.payload }

        default:
            return state;
    }
}

export {reducer, initialState}