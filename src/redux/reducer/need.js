import * as type from "../action/needType";

const initialState = {
    list: []
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case type.ADD_NEED:
            return { ...state, list: [...state.list, action.payload] }

        default:
            return state;
    }
}

export { reducer, initialState }