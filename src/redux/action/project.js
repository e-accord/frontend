import * as types from "./projectType";

const updateName = payload => {
    return {
        type: types.PROJECT_UPDATE_NAME,
        payload
    }
}

export  { updateName }