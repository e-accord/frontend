const PROJECT_SAVE = "project:save";
const PROJECT_UPDATE_NAME = 'project:update:name';

export { PROJECT_SAVE, PROJECT_UPDATE_NAME };