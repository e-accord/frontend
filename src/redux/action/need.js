import * as types from "./needType";

const addNeed = payload => {
    return {
        type: types.ADD_NEED,
        payload: {...payload, status: 'draft'}
    }
}

export { addNeed }