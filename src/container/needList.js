import React from "react";
import Item from "./../component/needItem";
import { connect } from "react-redux";

class NeedList extends React.Component {

    render() {

        return (
            this.props.needList.map( (need, index) => <Item need={need} key={index} /> )
        )
    }
}

const mapStateToProps = state => (
    {
        needList: state.need.list
    }
)

export default connect(mapStateToProps)(NeedList);